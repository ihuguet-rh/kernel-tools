#!/usr/bin/env python
"""Attempt to identify the state of a given MR's dependencies and mergeability."""

import argparse
import os
import subprocess
import tempfile

# python3-GitPython
import git
# python3-gitlab
import gitlab

projects = {"rhel-9": 24163133, "rhel-8": 24118165, "rhel-7": 24140364,
            "rhel-6": 24155748, "centos-stream-9": 24152864}

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--project', default="read_from_git")
parser.add_argument('-g', '--git-dir', default=os.getcwd())
parser.add_argument('-d', '--debug', required=False, default=False, action='store_true')
parser.add_argument('merge_request')
args = parser.parse_args()


def _git(uflags, **kwargs):
    kwargs.setdefault('check', True)
    return subprocess.run(['git'] + uflags,
                          cwd=args.git_dir, capture_output=True, **kwargs)


def _patch(uflags, **kwargs):
    return subprocess.run(['patch', '-p1', '-N', '--dry-run'] + uflags,
                          cwd=args.git_dir, capture_output=True, check=True, **kwargs)


try:
    _git(['status'])
except subprocess.CalledProcessError:
    print("This does not appear to be a valid git repository")
    exit(1)

repo = git.Repo(args.git_dir)
if args.project == "read_from_git":
    url = repo.remotes[0].config_reader.get("url")
    args.project = url.split("/")[-1].strip(".git")

if args.merge_request.startswith("http"):
    args.merge_request = args.merge_request.split("/")[-1]

# output = subprocess.check_output(['git', 'remote', 'get-url', 'origin'], cwd=args.git_dir, text=True)
output = subprocess.check_output(['git', 'remote', '-v'], cwd=args.git_dir, text=True)
# if output.split("/")[-1] != f'{args.project}.git':
if args.project not in output:
    print(f"Is this the right git tree? Project {args.project} remote not found in:")
    print(output.strip())
    exit(1)

gl = gitlab.Gitlab.from_config('gitlab.com')
gl_project = gl.projects.get(projects[args.project])
mr = gl_project.mergerequests.get(args.merge_request)
print(f'Working on {gl_project.name} MR {mr.iid}')
for line in mr.description.splitlines():
    if line.startswith("Depends: "):
        print(f' * {line}')

if mr.merge_status != "can_be_merged":
    print(f'Merge Request merge_status is {mr.merge_status}, rebase required')
    exit(1)

latest_start = None
labels = mr.labels
for label in labels:
    if label.startswith("Dependencies::"):
        dep_scope = label.split("::")[1]
        if dep_scope not in "OK":
            latest_start = dep_scope
        else:
            print(f"This set has no unmerged dependencies ({label})")
            exit(0)

if latest_start is None:
    print("This MR seems to be missing a Dependencies label, unable to continue")
    exit(1)

mr_commit_count = 0
for commit in mr.commits():
    if not commit.id.startswith(latest_start):
        mr_commit_count += 1
    else:
        break

dep_commit_count = len(mr.commits()) - mr_commit_count

local_head_commit = repo.head.commit
target_head_commit = None

# Make sure we actually have MR refs available in the git tree
mr_refs = 'refs/merge-requests/'
output = subprocess.check_output(['git', 'config', '--get-all', 'remote.origin.fetch'], cwd=args.git_dir, text=True)
if mr_refs not in output:
    print(f"Did not find {mr_refs} in git config output:\n  {output}")
    print(f"Please run:\n  git -C {args.git_dir} config --add remote.origin.fetch "
           "'+refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*'\nand try again")
    exit(1)

origin = repo.remotes.origin
for fetch_info in origin.fetch():
    if str(fetch_info.ref) == f"origin/{mr.target_branch}":
        target_head_commit = fetch_info.commit

if str(repo.active_branch) != str(mr.target_branch):
    print(f"Warning: local git branch '{repo.active_branch}' "
          f"doesn't match MR target branch '{mr.target_branch}'")

if local_head_commit != target_head_commit:
    print(f"Warning: your local git branch HEAD '{local_head_commit}' "
          f"does not match the remote target branch HEAD '{target_head_commit}'")

if args.debug:
    print(f"MR Start sha: {mr.diff_refs['start_sha']}")
    print(f"MR Head sha: {mr.diff_refs['head_sha']}")
    print(f"Deps sha: {latest_start}")
    print(f"Local HEAD: {local_head_commit}")
    print(f"Remote HEAD: {target_head_commit}")
    print(f"MR commit count: {mr_commit_count}")
    print(f"Dep commit count: {dep_commit_count}")

# Check if this is a merge commit, dep patch generation handling is different if it is
merge_commit_deps = False
dep_commit = gl_project.commits.get(latest_start)
if len(dep_commit.parent_ids) > 1:
    merge_commit_deps = True
    if args.debug:
        print(f'Firt dependency commit ({latest_start}) is a merge commit')

my_diff = repo.git.diff(latest_start, mr.diff_refs['head_sha'], '--no-renames')
if merge_commit_deps:
    dep_diff = repo.git.show(latest_start, '--first-parent')
else:
    dep_diff = repo.git.diff(mr.diff_refs['base_sha'], latest_start, '--no-renames')

fdep_diff = tempfile.NamedTemporaryFile(mode="w")
fmy_diff = tempfile.NamedTemporaryFile(mode="w")

fdep_diff.write("From: Dependency Checker <nobody@example.com>\n\n")
fdep_diff.write("%s\n" % dep_diff)
fdep_diff.flush()

fmy_diff.write("From: Dependency Checker <nobody@example.com>\n\n")
fmy_diff.write("%s\n" % my_diff)
fmy_diff.flush()

if args.debug:
    os.system("cp %s /tmp/mr-%s-%s-deps.diff" % (fdep_diff.name, args.project, args.merge_request))
    os.system("cp %s /tmp/mr-%s-%s-code.diff" % (fmy_diff.name, args.project, args.merge_request))

deps_merged = False
# if the dependencies apply cleanly, then they're obviously not merged
try:
    _patch(['-i', fdep_diff.name])
except subprocess.CalledProcessError:
    deps_merged = True

if not deps_merged:
    print("Dependencies do not appear to be merged yet")
    exit(0)

deps_clean = True
# if we can reverse-apply the patch cleanly, then deps should be clean, though it's possible
# there was additional code in the dependent MR, but that shouldn't matter, as we'll suggest
# a trivial rebase if there's dep code that doesn't match merged SHAs 100% anyway, and an
# interactive rebase if the MR's code doesn't apply.
if deps_merged:
    try:
        _patch(['-R', '-i', fdep_diff.name])
    except subprocess.CalledProcessError:
        deps_clean = False

if deps_clean:
    print("Dependencies are merged and appear to match what's in main")
else:
    print("Dependencies may be partially merged, interactive rebase advised")
    exit(1)

code_applies_cleanly = True
if deps_clean:
    try:
        _patch(['-i', fmy_diff.name])
    except subprocess.CalledProcessError:
        code_applies_cleanly = False

if code_applies_cleanly:
    # if dep_commit_count is 1, all we have is a remaining merge commit
    if merge_commit_deps and dep_commit_count <= 1:
        print("MR code applies cleanly, you should be all set")
    # otherwise, there's dependency code that was merged under a different SHA
    else:
        print("MR code applies cleanly, and can be trivially rebased")
else:
    print("MR code does not apply cleanly to main, interactive rebase advised")
    exit(1)

exit(0)
